2020-04-05 23:36:12.266 -05:00 [INF] Epoch: 0 | Accuracy 0.6341015 | Loss: 0
2020-04-05 23:36:12.421 -05:00 [INF] Epoch: 1 | Accuracy 0.5364056 | Loss: 0
2020-04-05 23:36:12.564 -05:00 [INF] Epoch: 2 | Accuracy 0.66635936 | Loss: 0
2020-04-05 23:36:12.700 -05:00 [INF] Epoch: 3 | Accuracy 0.69262666 | Loss: 0
2020-04-05 23:36:12.829 -05:00 [INF] Epoch: 4 | Accuracy 0.621659 | Loss: 0
2020-04-05 23:36:12.971 -05:00 [INF] Epoch: 5 | Accuracy 0.5202765 | Loss: 0
2020-04-05 23:36:13.101 -05:00 [INF] Epoch: 6 | Accuracy 0.6663594 | Loss: 0
2020-04-05 23:36:13.229 -05:00 [INF] Epoch: 7 | Accuracy 0.6682029 | Loss: 0
2020-04-05 23:36:13.364 -05:00 [INF] Epoch: 8 | Accuracy 0.6373273 | Loss: 0
2020-04-05 23:36:13.510 -05:00 [INF] Epoch: 9 | Accuracy 0.6474655 | Loss: 0
2020-04-05 23:36:13.645 -05:00 [INF] Epoch: 10 | Accuracy 0.64746535 | Loss: 0
2020-04-05 23:36:13.779 -05:00 [INF] Epoch: 11 | Accuracy 0.5857143 | Loss: 0
2020-04-05 23:36:13.891 -05:00 [INF] Epoch: 12 | Accuracy 0.55852526 | Loss: 0
2020-04-05 23:36:14.015 -05:00 [INF] Epoch: 13 | Accuracy 0.60921663 | Loss: 0
2020-04-05 23:36:14.129 -05:00 [INF] Epoch: 14 | Accuracy 0.61658984 | Loss: 0
2020-04-05 23:36:14.250 -05:00 [INF] Epoch: 15 | Accuracy 0.6617512 | Loss: 0
2020-04-05 23:36:14.371 -05:00 [INF] Epoch: 16 | Accuracy 0.6437789 | Loss: 0
2020-04-05 23:36:14.505 -05:00 [INF] Epoch: 17 | Accuracy 0.5880185 | Loss: 0
2020-04-05 23:36:14.630 -05:00 [INF] Epoch: 18 | Accuracy 0.60552996 | Loss: 0
2020-04-05 23:36:14.747 -05:00 [INF] Epoch: 19 | Accuracy 0.61059916 | Loss: 0
2020-04-05 23:36:14.861 -05:00 [INF] Epoch: 20 | Accuracy 0.6359447 | Loss: 0
2020-04-05 23:36:14.971 -05:00 [INF] Epoch: 21 | Accuracy 0.6442397 | Loss: 0
2020-04-05 23:36:15.078 -05:00 [INF] Epoch: 22 | Accuracy 0.66313374 | Loss: 0
2020-04-05 23:36:15.183 -05:00 [INF] Epoch: 23 | Accuracy 0.64055294 | Loss: 0
2020-04-05 23:36:15.295 -05:00 [INF] Epoch: 24 | Accuracy 0.63133633 | Loss: 0
2020-04-05 23:36:15.420 -05:00 [INF] Epoch: 25 | Accuracy 0.632719 | Loss: 0
2020-04-05 23:36:15.545 -05:00 [INF] Epoch: 26 | Accuracy 0.6502304 | Loss: 0
2020-04-05 23:36:15.672 -05:00 [INF] Epoch: 27 | Accuracy 0.63179725 | Loss: 0
2020-04-05 23:36:15.785 -05:00 [INF] Epoch: 28 | Accuracy 0.62764984 | Loss: 0
2020-04-05 23:36:15.914 -05:00 [INF] Epoch: 29 | Accuracy 0.6152074 | Loss: 0
2020-04-05 23:36:16.706 -05:00 [INF] Elapsed time: 00:00:49.23
2020-04-05 23:36:17.734 -05:00 [INF] Classifying single image
2020-04-05 23:36:17.736 -05:00 [INF] Image: Severe133.jpg | Actual Value: severe | Predicted Value: moderate
2020-04-05 23:36:17.738 -05:00 [INF] Classifying multiple images
2020-04-05 23:36:18.653 -05:00 [INF] Image: Severe133.jpg | Actual Value: severe | Predicted Value: moderate
2020-04-05 23:36:18.654 -05:00 [INF] Image: Severe62.jpg | Actual Value: severe | Predicted Value: severe
2020-04-05 23:36:18.654 -05:00 [INF] Image: Minor55.jpg | Actual Value: minor | Predicted Value: minor
2020-04-05 23:36:18.655 -05:00 [INF] Image: Severe132.jpg | Actual Value: severe | Predicted Value: minor
2020-04-05 23:36:18.656 -05:00 [INF] Image: Severe142.jpg | Actual Value: severe | Predicted Value: moderate
2020-04-05 23:36:18.657 -05:00 [INF] Image: Severe241.jpg | Actual Value: severe | Predicted Value: severe
2020-04-05 23:36:18.658 -05:00 [INF] Image: Minor314.jpg | Actual Value: minor | Predicted Value: minor
2020-04-05 23:36:18.659 -05:00 [INF] Image: Severe246.jpg | Actual Value: severe | Predicted Value: severe
2020-04-05 23:36:18.659 -05:00 [INF] Image: Severe53.jpg | Actual Value: severe | Predicted Value: severe
2020-04-05 23:36:18.660 -05:00 [INF] Image: Severe218.jpg | Actual Value: severe | Predicted Value: severe
