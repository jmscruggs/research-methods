import numpy as np
import time
from keras.preprocessing.image import ImageDataGenerator, img_to_array, load_img
from keras.models import Sequential, Model
from keras.layers import Dropout, Flatten, Dense, GlobalAveragePooling2D
from keras import applications
from keras.utils.np_utils import to_categorical
from keras.callbacks import EarlyStopping, LambdaCallback, ModelCheckpoint
import matplotlib.pyplot as plt
from keras.optimizers import RMSprop,SGD
import logging
import math
import cv2
import os


# dimensions of our images.
img_width, img_height = 224, 224

top_model_weights_path = 'bottleneck_fc_model.h5'
BASE_DIR    = os.path.dirname(os.path.abspath(__file__))
train_data_dir = "{a1}\{a2}".format(a1 = BASE_DIR, a2 = r'..\assets2\training')
validation_data_dir = "{a1}\{a2}".format(a1 = BASE_DIR, a2 = r'..\assets2\validation')


logging.basicConfig(level=logging.INFO, filename='mobilenet-python.log', filemode='w', format='%(asctime)s - %(message)s')

print(train_data_dir)
print(validation_data_dir)
# number of epochs to train top model
epochs = 30
# batch size used by flow_from_directory and predict_generator
batch_size = 10

def time_convert(sec):
  mins = sec // 60
  sec = sec % 60
  hours = mins // 60
  mins = mins % 60
  print("Time Lapsed = {0}:{1}:{2}".format(int(hours),int(mins),sec))
  logging.info("Time Lapsed = {0}:{1}:{2}".format(int(hours),int(mins),sec))


def save_bottlebeck_features():
    # build the InceptionV3 network
    model = applications.MobileNetV2(include_top=False, weights='imagenet')

    datagen = ImageDataGenerator(rescale=1. / 255)

    generator = datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode=None,
        shuffle=False)

    nb_train_samples = len(generator.filenames)
    print("Number of training samples: " + str(nb_train_samples))
    num_classes = 3

    predict_size_train = int(math.ceil(nb_train_samples / batch_size))

    print("At bottleneck_features_train...")
    logging.info('At bottleneck_features_train...')
    bottleneck_features_train = model.predict_generator(
        generator, predict_size_train)

    print("End bottleneck_features_train")
    logging.info('End bottleneck_features_train...')

    np.save('bottleneck_features_train.npy', bottleneck_features_train)

    generator = datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode=None,
        shuffle=False)

    nb_validation_samples = len(generator.filenames)

    predict_size_validation = int(
        math.ceil(nb_validation_samples / batch_size))

    print("At bottleneck_features_validation")
    logging.info('At bottleneck_features_validation...')
    bottleneck_features_validation = model.predict_generator(
        generator, predict_size_validation)
    print("End bottleneck_features_validation...")
    logging.info('End bottleneck_features_validation...')


    np.save('bottleneck_features_validation.npy',
            bottleneck_features_validation)

def train_top_model(time, start_time):
    datagen_top = ImageDataGenerator(rescale=1. / 255)
    generator_top = datagen_top.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode='categorical',
        shuffle=False)

    nb_train_samples = len(generator_top.filenames)
    num_classes = 3

    # save the class indices to use use later in predictions
    np.save('class_indices.npy', generator_top.class_indices)

    # load the bottleneck features saved earlier
    train_data = np.load('bottleneck_features_train.npy')

    # get the class lebels for the training data, in the original order
    train_labels = generator_top.classes

    # https://github.com/fchollet/keras/issues/3467
    # convert the training labels to categorical vectors
    train_labels = to_categorical(train_labels, num_classes=num_classes)

    generator_top = datagen_top.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode=None,
        shuffle=False)

    nb_validation_samples = len(generator_top.filenames)

    validation_data = np.load('bottleneck_features_validation.npy')

    validation_labels = generator_top.classes
    validation_labels = to_categorical(
        validation_labels, num_classes=num_classes)

    print("train data shape = ")
    print(train_data.shape[1:])

    model = Sequential()
    model.add(Flatten(input_shape=train_data.shape[1:]))
    model.add(Dense(256, activation='relu'))
    model.add(Dense(num_classes, activation='softmax'))

    model.compile(optimizer=SGD(lr=0.01,momentum=0.0, nesterov=False),
                  loss='categorical_crossentropy', metrics=['accuracy'])

    print("=======================model summary======================")
    print(model.summary())

    batch_print_callback = LambdaCallback(
          on_epoch_end = lambda epoch, logs: logging.info({'Epoch': epoch, 'Accuracy': logs['val_accuracy'], 'Loss': logs['val_loss']}))

    earlystop = EarlyStopping(
                          monitor='val_accuracy',
                          min_delta=0,
                          patience=30,
                          restore_best_weights=True)

    history = model.fit(train_data, train_labels,
                        epochs=epochs,
                        batch_size=batch_size,
                        validation_data=(validation_data, validation_labels),
                        callbacks=[earlystop, batch_print_callback])

    print("done here --------------------")
    model.save_weights(top_model_weights_path)

    (eval_loss, eval_accuracy) = model.evaluate(
        validation_data, validation_labels, batch_size=batch_size, verbose=1)

    print("[INFO] accuracy: {:.2f}%".format(eval_accuracy * 100))
    print("[INFO] Loss: {}".format(eval_loss))

    logging.info("[INFO] accuracy: {:.2f}%".format(eval_accuracy * 100))
    logging.info("[INFO] Loss: {}".format(eval_loss))


    end_time = time.time()
    time_lapsed = end_time - start_time
    time = time_convert(time_lapsed)

    plt.figure(1)

    # summarize history for accuracy

    plt.subplot(211)
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')

    # summarize history for loss

    plt.subplot(212)
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()


def predict():
    # load the class_indices saved in the earlier step
    class_dictionary = np.load('class_indices.npy', allow_pickle=True).item()

    num_classes = 3

    # add the path to your test image below
    image_path = r'C:\Users\Scruggs\research-methods\assets2\validation\moderate\Moderate1.JPG'

    orig = cv2.imread(image_path)

    print("[INFO] loading and preprocessing image...")
    logging.info("[INFO] loading and preprocessing image...")

    image = load_img(image_path, target_size=(224, 224))
    image = img_to_array(image)

    # important! otherwise the predictions will be '0'
    image = image / 255

    image = np.expand_dims(image, axis=0)

    # build the VGG16 network
    model = applications.MobileNetV2(include_top=False, weights='imagenet')

    # get the bottleneck prediction from the pre-trained VGG16 model
    bottleneck_prediction = model.predict(image)

    # build top model
    model = Sequential()
    model.add(Flatten(input_shape=bottleneck_prediction.shape[1:]))
    model.add(Dense(256, activation='relu'))
    model.add(Dense(num_classes, activation='softmax'))

    model.load_weights(top_model_weights_path)


    # use the bottleneck prediction on the top model to get the final
    # classification
    class_predicted = model.predict_classes(bottleneck_prediction)

    probabilities = model.predict_proba(bottleneck_prediction)

    inID = class_predicted[0]

    inv_map = {v: k for k, v in class_dictionary.items()}

    label = inv_map[inID]

    # get the prediction label
    print("Image ID: {}, Label: {}".format(inID, label))
    logging.info("Image ID: {}, Label: {}".format(inID, label))


    # display the predictions with the image
    cv2.putText(orig, "Predicted: {}".format(label), (10, 30),
                cv2.FONT_HERSHEY_PLAIN, 1.5, (43, 99, 255), 2)

    cv2.imshow("Classification", orig)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

start_time = time.time()

save_bottlebeck_features()
train_top_model(time, start_time)
predict()


cv2.destroyAllWindows()